import { environment } from "../config/index"
import Home from "app/views/Home.vue";
import Dashboard from "app/views/Dashboard.vue";
import ShippingRequest from "app/views/ShippingRequest.vue";
import Test from "app/views/Test.vue";
import NotFound from "app/views/NotFound.vue";
import LoginV from "app/views/LoginV";

const routes = {
    mode: "history",
    base: environment.url.subdirectory,
    routes: [
        {
            path: "/",
            name: "home",
            component: Home,
            meta: {
                requiresVisitor: true
            }
        },
        {
            path: "/login",
            name: "login",
            component: LoginV,
            meta: {
                requiresVisitor: true
            }
        },
        {
            path: "/test",
            name: "test",
            component: Test
        },
        {
            path: "*",
            name: "404",
            component: NotFound
        },
        {
            path: "/dashboard",
            name: "dashboard",
            component: Dashboard,
            meta: {
                requiresAuth: true,
            }
        },
        {
            path: "/shipping-request",
            name: "shipping-request",
            component: ShippingRequest,
            meta: {
                requiresAuth: true,
            }
        },
    ]
};

export default routes;

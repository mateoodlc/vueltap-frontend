/**
 * Created by mendieta on 11/7/16.
 */

import AuthManager from "foo/net/AuthManager";
import axios from 'axios';

export const LOGIN = "user/login";
export const REGISTER = "user/register";
export const LOGOUT = "user/logout";
export const LOGGING_IN = "user/logging_in";

const state = {
    token: localStorage.getItem('access_token') || null,
    logged: false,
    logging: false,
    facebook: null,
    google: null,
    xeerpa: null,
    api: null
};

const actions = {
    
    getToken(context, credentials){
        
        return new Promise((resolve, reject) => {
        
        axios.put('http://api.vueltap.com.co:8080/api_1.0/User/Login', {
            email: credentials.username,
            password: credentials.password
        })
        .then(response => {

            console.log(":::" + response.data.status)

            

             if(response.data.status === true){
                
                const token = response.data.response.session.token
                localStorage.setItem('access_token', token)
                context.commit('getToken', token)
                context.commit('logIn', true)

            }else{

                console.log("usuario no encontrado");
            }
            

           // commit(LOGGING_IN, true);
        
            /*console.log(response)
            console.log(response.status)
            console.log("token: " + response.data.response.session.token)*/

            resolve(response)
        })
        .catch(err => {

            console.log(err)
            reject(err)
            
        })
    })

    },
    logout(context){

        return new Promise((resolve) => {

            localStorage.removeItem('access_token')
            context.commit('logOut')
            context.commit('logIn', false)

            resolve(resolve)

        })

    },
    [LOGIN]({ commit }, payload) {
        return new Promise((resolve, reject) => {
            commit(LOGGING_IN, true);
            AuthManager.login(payload.service, payload.data, payload.xs)
                .then((response) => {
                    commit(LOGIN, { network: payload.service, response });
                    resolve(response);
                    commit(LOGGING_IN, false);
                })
                .catch((error) => {
                    commit(LOGGING_IN, false);
                    reject(error);
                });
        });
    },
    [REGISTER]({ commit }, payload) {
        commit(REGISTER, payload);
    },
    [LOGOUT]({ commit }, payload) {
        commit(LOGOUT, payload);
    }
};

const mutations = {
    [LOGIN](state, payload) {
        state.logged = true;
        state[payload.network] = payload.response;
    },
    [LOGGING_IN](state, payload) {
        state.logging = payload;
    },
    [REGISTER](state, data) {
        state.error = null;
        state.register_data = data;
    },
    [LOGOUT](state) {
        state.error = null;
        state.logged = false;
    },
    getToken(state, token){
        state.token = token;
    },
    logOut(state){
        state.token = null;
    },
    logIn(state, data){
        state.logged = data;
    }
};

const getters = {
    loggedIn: state => {
        return state.logged;
    }
};

export default { state, actions, mutations, getters };
